package main

import (
	"io"
	"log"
	"net"
	"net/http"
)

type DialFunc func(n,a string) (net.Conn,error)

type ProxyServer struct {
	Dial DialFunc
	Transport http.RoundTripper
}

func (s *ProxyServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	r.Header.Del("Proxy-Authorization")

	if r.Method == "CONNECT" {
		s.handleTunneling(w, r)
	} else if r.URL.Host == "" {
		http.NotFound(w,r)
	} else {
		s.handleHttpProxy(w, r)
	}
}

func (s *ProxyServer) handleHttpProxy(w http.ResponseWriter, r *http.Request) {
	resp,err := s.Transport.RoundTrip(r)
	if resp != nil { defer resp.Body.Close() }

	if err != nil {
		log.Println(r.Method, r.URL, err)
		w.WriteHeader(http.StatusBadGateway)
		log.Println(r.Method, r.URL, err.Error())
	} else {
		log.Println(r.Method, r.URL, resp.Status)
		CopyHeader(w.Header(), resp.Header)
		w.WriteHeader(resp.StatusCode)
		io.Copy(w, resp.Body)
	}
}

func (s *ProxyServer) handleTunneling(w http.ResponseWriter, r *http.Request) {
	var err error
	defer func() {
		if err != nil {
			log.Println(r.Method, r.RequestURI, err)
			http.Error(w, err.Error(), http.StatusBadGateway)
		}
	}()

	oconn,err := s.Dial("tcp", r.RequestURI)
	if oconn != nil { defer oconn.Close() }
	if err != nil { return }

	w.WriteHeader(http.StatusOK)
	iconn, _, err := w.(http.Hijacker).Hijack()
	if iconn != nil { defer iconn.Close() }
	if err != nil { return }

	log.Println(r.Method, r.URL, "OK")
	closed := make(chan error, 2)
	go func(){
		_,err = io.Copy(iconn, oconn)
		closed <- err
	}()
	go func(){
		_,err = io.Copy(oconn, iconn)
		closed <- err
	}()
	<-closed
}

func CopyHeader(dst, src http.Header) {
	for key,values := range src {
		for _,value := range values {
			dst.Add(key,value)
		}
	}
}
