package main

import (
	"flag"
	"log"
	"net"
	"net/http"
	"time"
	"golang.org/x/net/proxy"
)

var (
	argForwardSocks5 = flag.String("forward-socks5", "", "")
	argHttpListen = flag.String("http-listen", "localhost:3128", "")
)

func main() {
	flag.Parse()

	if *argForwardSocks5 == "" {
		log.Fatal("-forward-socks5 required")
	}

	dialer := &net.Dialer{
		Timeout:   30 * time.Second,
		KeepAlive: 30 * time.Second,
	}
	//TODO:鉴权支持
	socks5Dialer,err := proxy.SOCKS5("tcp",
		*argForwardSocks5, nil, dialer)
	if err != nil { log.Fatal(err) }
	timeoutSocks5Dialer := &TimeoutDialer{
		DialFunc : socks5Dialer.Dial,
		Timeout : time.Minute * 5, //TODO: 超时参数
	}

	proxyServer := &ProxyServer{
		Dial : timeoutSocks5Dialer.Dial,
		Transport : &http.Transport{
			Proxy : http.ProxyURL(nil),
			Dial : timeoutSocks5Dialer.Dial,
			//不需要ResponseHeaderTimeout，Dial已经包含超时功能
		},
	}

	httpServer := &http.Server{
		Addr : *argHttpListen,
		Handler : proxyServer,
		ReadTimeout : time.Minute * 5, //TODO: 超时参数
		WriteTimeout : time.Minute * 5, //TODO: 超时参数
	}

	log.Fatal(httpServer.ListenAndServe())
}
