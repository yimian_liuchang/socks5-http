package main

import (
	"net"
	"time"
)

//TimeoutConn包含一个内部的Conn，设置每个请求的最低超时时间
type TimeoutConn struct {
	net.Conn
	Timeout time.Duration

	//private
	readDeadline, writeDeadline time.Time
}

func (c *TimeoutConn) deadline() time.Time {
	return time.Now().Add(c.Timeout)
}

func (c *TimeoutConn) Read(b []byte) (n int, err error) {
	if c.readDeadline.IsZero() {
		c.Conn.SetReadDeadline(c.deadline())
	}
	return c.Conn.Read(b)
}

func (c *TimeoutConn) Write(b []byte) (n int, err error) {
	if c.writeDeadline.IsZero() {
		c.Conn.SetWriteDeadline(c.deadline())
	}
	return c.Conn.Write(b)
}

func (c *TimeoutConn) SetDeadline(t time.Time) error {
	if d := c.deadline() ; t.After(d) { t = d }
	c.readDeadline = t
	c.writeDeadline = t
	return c.Conn.SetDeadline(t)
}

func (c *TimeoutConn) SetReadDeadline(t time.Time) error {
	if d := c.deadline() ; t.After(d) { t = d }
	c.readDeadline = t
	return c.Conn.SetReadDeadline(t)
}

func (c *TimeoutConn) SetWriteDeadline(t time.Time) error {
	if d := c.deadline() ; t.After(d) { t = d }
	c.writeDeadline = t
	return c.Conn.SetWriteDeadline(t)
}

//所有发起的连接都用TimeoutConn包装
type TimeoutDialer struct {
	DialFunc func(network, host string) (net.Conn, error)
	Timeout time.Duration
}

func (d *TimeoutDialer) Dial(n, host string) (net.Conn, error) {
	conn, err := d.DialFunc(n, host)
	if conn != nil {
		conn = &TimeoutConn{
			Conn : conn,
			Timeout : d.Timeout,
		}
	}
	return conn, err
}
